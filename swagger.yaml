swagger: '2.0'
info:
  title: GofusApi
  description: Serve accounts, characters and more.
  version: 0.0.1
tags:
  - name: Accounts
    description: Everything about your Accounts
  - name: Characters
    description: Everything about your Characters

paths:
  /accounts:
    get:
      summary: "Return all accounts"
      operationId: "getAccounts"
      tags:
      - Accounts
      produces:
      - application/json
      responses:
        200:
          description: A list of account
          schema:
            type: array
            items:
              $ref: '#/definitions/Account'
          examples:
            application/json:
              - name: "Vegeta"
                password: null
                lastConnexion: '10/11/2019 (15:23:03)'
                state: DISCONNECTED
              - name: "bestFeca"
                password: null
                lastConnexion: '10/11/2019 (15:23:03)'
                state: DISCONNECTED
    post:
      summary: "Creates a new account"
      operationId: "createAccount"
      tags:
      - Accounts
      consumes:
      - application/json
      parameters:
        - in: body
          name: account
          description: The account to create.
          schema:
            $ref: '#/definitions/Account'
      responses:
        201:
          description: Successfully created new account
        409:
          description: Conflict, an account with the same name already exists
        422: 
          description: Missing required property in body
  /accounts/{accountName}:
    get:
      summary: "Return an existing account from name"
      operationId: "getAccount"
      tags:
      - Accounts
      produces:
      - application/json
      parameters:
      - $ref: '#/parameters/accountName'
      responses:
        200:
          description: Success
          schema:
            $ref: "#/definitions/Account"
          examples:
            application/json:
              name: "Vegeta"
              password: null
              lastConnexion: '10/11/2019 (15:23:03)'
              state: DISCONNECTED
        404:
          description: Error, the specified account doesn't exists
    patch:
      summary: "Update an existing account"
      operationId: "updateAccount"
      tags:
      - Accounts
      consumes:
      - application/json
      parameters:
        - $ref: '#/parameters/accountName'
        - in: body
          name: account
          description: The account to update.
          schema:
            $ref: '#/definitions/Account'
      responses:
        200:
          description: Successfully updated account
        304:
          description: Not modified, account is already the same object
        404:
          description: Error, the specified account doesn't exists
        422: 
          description: Missing required property in body
  /accounts/{accountName}/characters:
    get:
      summary: "Return characters list for specified account"
      operationId: "getAccountCharacters"
      tags:
      - Accounts
      produces:
      - application/json
      parameters:
      - $ref: '#/parameters/accountName'
      - $ref: '#/parameters/gameServerIdFilter'
      responses:
        200:
          description: A list of characters for this account
          schema:
            type: array
            items:
              $ref: '#/definitions/Character'
          examples:
            application/json:
              - id: "80077248"
                accountName: Vegeta
                name: VegetaPegasus
                class: FECA
                gender: M
                level: 198
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 608
              - id: "80077232"
                accountName: Vegeta
                name: Sangoku
                class: IOP
                gender: M
                level: 200
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 608
              - id: "80077231"
                accountName: Vegeta
                name: Sangohan
                class: CRA
                gender: M
                level: 145
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 607
        404:
          description: Error, the specified account doesn't exists
  /accounts/{accountName}/retrieveCharacters/{gameServerId}:
    get:
      summary: "Refresh characters list for specified account and gameServerId"
      operationId: "retrieveAccountCharacters"
      tags:
      - Accounts
      produces:
      - application/json
      parameters:
      - $ref: '#/parameters/accountName'
      - name: gameServerId
        in: path
        description: GameServerId
        required: true
        type: integer
      responses:
        200:
          description: Account's characters list for the specified gameServerId
          schema:
            type: array
            items:
              $ref: '#/definitions/Character'
          examples:
            application/json:
              - id: "80077248"
                accountName: Vegeta
                name: VegetaPegasus
                class: FECA
                gender: M
                level: 198
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 608
              - id: "80077232"
                accountName: Vegeta
                name: Sangoku
                class: IOP
                gender: M
                level: 200
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 608
        404:
          description: Error, the specified account doesn't exists
        406:
          description: Invalid gameServerId
        412:
          description: Unable to connect to the specified
  /characters:
    get:
      summary: "Return all characters"
      operationId: "getCharacters"
      tags:
      - Characters
      produces:
      - application/json
      parameters:
      - $ref: '#/parameters/accountNameFilter'
      - $ref: '#/parameters/gameServerIdFilter'
      responses:
        200:
          description: A list of characters
          schema:
            type: array
            items:
              $ref: '#/definitions/Character'
          examples:
            application/json:
              - id: "80077248"
                accountName: Vegeta
                name: VegetaPegasus
                class: FECA
                gender: M
                level: 198
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 608
              - id: "80077232"
                accountName: Vegeta
                name: Sangoku
                class: IOP
                gender: M
                level: 200
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 608
              - id: "80077231"
                accountName: Vegeta
                name: Sangohan
                class: CRA
                gender: M
                level: 145
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 607
              - id: "80077287"
                accountName: bestFeca
                name: Bastila
                class: FECA
                gender: F
                level: 200
                isDead: false
                connexionState: DISCONNECTED
                gameState: UNDEFINED
                colors:
                  - 45ab32
                  - 59ff7b
                  - 65de21
                gameServerId: 607
              
        404:
          description: Error, at least one of the specified accounts doesn't exists
        406:
          description: Error, at least one one of the specified gameServerId is invalid
  /characters/{characterId}:
    get:
      summary: "Return an existing character from Id"
      operationId: "getCharacter"
      tags:
      - Characters
      produces:
      - application/json
      parameters:
      - $ref: '#/parameters/characterId'
      responses:
        200:
          description: Success
          schema:
            $ref: '#/definitions/Character'
          examples:
            application/json:
              id: "80077248"
              accountName: Vegeta
              name: VegetaPegasus
              class: FECA
              gender: M
              level: 198
              isDead: false
              connexionState: DISCONNECTED
              gameState: UNDEFINED
              colors:
                - 45ab32
                - 59ff7b
                - 65de21
              gameServerId: 608
        404:
          description: Error, the specified character doesn't exists

parameters:
  accountName:
    name: accountName
    in: path
    description: "The account's name for which to retrieve the characters"
    required: true
    type: string
  characterId:
    name: characterId
    in: path
    description: "The character's Id used to retrieve the character"
    required: true
    type: string
  accountNameFilter:
    name: accountNameFilter
    in: query
    type: array
    items:
      type: string
    uniqueItems: true
    description: "An optional value to filter the returned character list by accounts"
  gameServerIdFilter:
    name: gameServerIdFilter
    in: query
    type: array
    items:
      type: integer
    uniqueItems: true
    description: "An optional value to filter the returned character list by gameServerIds"
definitions:
  Account:
    type: object
    properties:
      name: 
        type: string
        example: "myAccountName"
      password:
        type: string
        example: "myPassword"
      lastConnexion:
        type: string
        example: "10/11/2019 (15:23:03)"
      state: 
        type: string
        enum:
        - BANNED
        - BAD_PASSWORD
        - DISCONNECTED
        - CONNECTED
    example:   # <-----
      name: "myAccountName"
      password: "myPassword"
      lastConnexion: "10/11/2019 (15:23:03)"
      state: DISCONNECTED
    required:
    - name
    - password
  Character:
    type: object
    properties:
      id:
        type: string
        description: "The character's Id"
        example: "80077248"
      accountName:
        type: string
        example: "characterAccountName"
        description: "The character's account name"
      name:
        type: string
        example: "myCharacterName"
        description: "The character's name"
      class: 
        type: string
        description: "The character's class"
        enum:
        - FECA
        - IOP
        - OSAMODAS
        - ENUTROF
        - CRA
        - SACRIEUR
        - XELOR
        - SRAM
        - ENIRIPSA
        - ECAFLIP
        - PANDAWA
        - SADIDA
      gender:
        type: string
        enum:
        - M
        - F
        description: "The character's gender"
      level:
        type: integer
        example: 42
        description: "The character's level"
      isDead:
        type: boolean
        example: false
        description: "A boolean indicating if the character is dead on heroic servers"
      connexionState:
        type: string
        enum:
        - MERCHANT
        - DISCONNECTED
        - CONNECTED
        example: DISCONNECTED
        description: "The character's connexion state"
      gameState:
        type: string
        enum:
        - UNDEFINED
        - IDLE
        - FIGHTING
        - GATHERING
        - RECOVERING
        - MOVING
      colors:
        type: array
        description: "An array of 3 strings which represents the character's colors"
        items:
          type: string
          minLength: 3
          maxLength: 3
        example:
        - "45ab32"
        - "59ff7b"
        - "65de21"
      gameServerId:
        type: integer
        example: 608
        description: "The character's game server id"