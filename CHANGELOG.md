# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1]

### Added
 - Models:
    - Account
    - Character
 - Operations:
    - Retrieve accounts (GET /accounts)
    - Retrieve account from name (GET /accounts/{accountName})
    - Create account (POST /accounts)
    - Update account (PATCH /accounts/{accountName})
    - Retrieve account characters (GET /accounts/{accountName}/characters)
### Changed
- Models:
    - Account
        - Renamed property `accountName` -> `name`
### Removed
